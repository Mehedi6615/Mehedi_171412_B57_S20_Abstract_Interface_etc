<?php

    abstract class MyAbstractClass{
        protected $name;

        abstract public function getData();
        abstract public function setData($value);

        public function displayData(){
            echo "Name: ".$this->name;
        }
    }

//end of myAbstractClass


class MyClass extends MyAbstractClass{
    public function getData()
    {
        return $this->name;
    }
    public function setData($value)
    {
        $this->name = $value;
    }
}//end of MyClass


$o = new MyClass();
$o->setData("BASIS BITM");

echo $o->displayData();