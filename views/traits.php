<?php
/*
trait Hello {
    public function sayHello() {
        echo 'Hello ';
    }
}

trait World {
    public function sayWorld() {
        echo 'World';
    }
}

class MyHelloWorld {
    use Hello, World;
    public function sayExclamationMark() {
        echo '!';
    }
}

$o = new MyHelloWorld();
$o->sayHello();
$o->sayWorld();
$o->sayExclamationMark();


*/



trait A {
    public function smallTalk() {
        echo 'a';
    }
    public function mediumTalk() {
        echo 'aA';
    }

    public function bigTalk() {
        echo 'A';
    }
}

trait B {
    public function smallTalk() {
        echo 'b';
    }
    public function mediumTalk() {
        echo 'bB';
    }
    public function bigTalk() {
        echo 'B';
    }
}


trait C {
    public function smallTalk() {
        echo 'c';
    }
    public function mediumTalk() {
        echo 'cC';
    }
    public function bigTalk() {
        echo 'C';
    }
}


class MyClass {
    use A,B,C{
        B::smallTalk insteadof A,C;
        A::bigTalk insteadof B,C;
        C::mediumTalk insteadof A,B;
        A::bigTalk as talkA;
    }

}


$o = new MyClass();
$o->smallTalk();
$o->bigTalk();
$o->mediumTalk();
$o->talkA();


?>
